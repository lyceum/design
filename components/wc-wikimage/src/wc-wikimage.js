/* eslint no-undef: 0 */

// const xmlns = 'http://www.w3.org/2000/svg'
const template = document.createElement('template')
template.innerHTML = `
<style>
  figure {
    border: thin #c0c0c0 solid;
    border-radius: 0.7em;
  }

  img {
    border-radius: 0.7em 0.7em 0 0;
    display: block;
    width: 100%;
  }

  a {
    color: var(--wc-color-anchor, #5f7974);
    text-decoration: none;
  }

  a:hover {
    color: var(--wc-color-anchor-hover, #89acc7);
    text-decoration: underline;
  }

  #caption {
    background-color: #222;
    color: #fff;
    font: italic smaller sans-serif;
    text-align: center;
  }
  
  #credit {
    background-color: rgba(255, 255, 255, 0.7);
    border-radius: 0 0 0.7em 0.7em;
    color: black;
    font-size: 0.7em;
    margin: 0 0.7em;
    text-align: right;
  }
</style>
<figure>
  <img />
  <figcaption id="caption"></figcaption>
  <figcaption id="credit">
    <a id="author"></a>&nbsp; <a id="licence"></a>&nbsp;
    <a id="image-description">via Wikimedia Commons</a>
  </figcaption>
</figure>
`

export class WCwikimage extends HTMLElement {
  constructor () {
    super()
    this.attachShadow({ mode: 'open' })
    // this.data = {}
    this.caption = ''
  }

  static get observedAttributes () {
    return ['title', 'caption']
  }

  attributeChangedCallback (name, oldValue, newValue) {
    if (oldValue !== newValue) {
      this[name] = newValue
    }
  }

  get title () {
    return this.getAttribute('title')
  }

  set title (title) {
    this.setAttribute('title', title)
  }

  get caption () {
    return this.getAttribute('caption')
  }

  set caption (caption) {
    this.setAttribute('caption', caption)
  }

  async fetchData (title) {
    const wkId = `Image:${title}`
    // from https://github.com/wikimedia/mediawiki-api-demos/blob/master/javascript/get_imageinfo.js

    // fetch metadata
    let url = 'https://commons.wikimedia.org/w/api.php'
    // https://www.mediawiki.org/wiki/API:Imageinfo
    // https://commons.wikimedia.org/w/api.php?origin=*&action=quer…geinfo&iiprop=extmetadata&titles=Image:Bistability_graph.svg
    // metadata
    const params = {
      action: 'query',
      format: 'json',
      prop: 'imageinfo',
      iiprop: 'extmetadata',
      titles: wkId
    }

    url = url + '?origin=*'
    Object.keys(params).forEach(function (key) {
      url += '&' + key + '=' + params[key]
    })

    const metadata = await fetch(url)
      .then(function (response) {
        return response.json()
      })
      .then(function (response) {
        const pages = response.query.pages
        // get first image id
        const imageId = Object.keys(pages)[0]
        const image = pages[imageId]
        const metadata = image.imageinfo[0].extmetadata
        return metadata
        // // get data
        // const data = {}
        // data.Copyrighted = metadata.Copyrighted.value
        // data.Licence = metadata.LicenseShortName.value
        // data.Attribution = metadata.Attribution.value
        // return data
      })
      .catch(function (error) {
        console.error(error)
      })

    // fetch thumbnails info
    const thumbnails = await fetch(
      `https://api.wikimedia.org/core/v1/commons/file/${wkId}`
    )
      .then(function (response) {
        return response.json()
      })
      .then(function (response) {
        return response
      })
      .catch(function (error) {
        console.error(error)
      })
    // merge objects
    return Object.assign(metadata, thumbnails)
  }

  async connectedCallback () {
    this.style.display = 'block'
    const data = await this.fetchData(this.title)

    // work on the DocumentFragment content before mounting it
    const fragment = template.content

    // image
    const img = fragment.querySelector('img')
    // prefer svg over png as it allows translations
    if (data.original.url.endsWith('.svg')) {
      img.src = data.original.url
    } else {
      // TODO Handle different sizes with media-queries
      img.src = data.preferred.url
    }
    // TODO Ugly handle description one time for all
    if (this.caption) {
      img.alt = this.caption
    } else if (data.ImageDescription) {
      img.alt = data.ImageDescription.value
    } else if (data.ObjectName) {
      img.alt = data.ObjectName.value
    } else {
      img.alt = 'Image libre issue de Wikimedia commons'
    }

    // attribution
    fragment.querySelector('#image-description').href =
      data.file_description_url
    const licence = fragment.querySelector('#licence')
    // public domain has no url link to french wikipédia

    if (data.LicenseUrl && data.LicenseUrl.value) {
      licence.href = data.LicenseUrl.value
    } else if (data.License && data.License.value === 'pd') { licence.href = 'https://fr.wikipedia.org/wiki/Domaine_public_(propri%C3%A9t%C3%A9_intellectuelle)' }

    licence.innerHTML = data.LicenseShortName.value
    // author
    const author = fragment.querySelector('#author')
    if (data.AttributionRequired.value === 'true') {
      author.innerHTML = '&copy;&nbsp;'

      if (data.Attribution && data.Attribution.value) {
        author.innerHTML += data.Attribution.value
      }
      //   else if (data.Artist) {
      //     author.innerHTML += data.Artist.value }
    } else {
      author.innerHTML = ''
    }

    // caption
    const caption = fragment.querySelector('#caption')
    if (this.caption) {
      caption.innerHTML = this.caption
    } else {
      caption.innerHTML = ''
    }
    // mount template
    this.shadowRoot.appendChild(fragment.cloneNode(true))
  }
}

customElements.define('wc-wikimage', WCwikimage)
