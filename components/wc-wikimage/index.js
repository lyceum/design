var s=document.createElement("template");s.innerHTML=`
<style>
  figure {
    border: thin #c0c0c0 solid;
    border-radius: 0.7em;
  }

  img {
    border-radius: 0.7em 0.7em 0 0;
    display: block;
    width: 100%;
  }

  a {
    color: var(--wc-color-anchor, #5f7974);
    text-decoration: none;
  }

  a:hover {
    color: var(--wc-color-anchor-hover, #89acc7);
    text-decoration: underline;
  }

  #caption {
    background-color: #222;
    color: #fff;
    font: italic smaller sans-serif;
    text-align: center;
  }
  
  #credit {
    background-color: rgba(255, 255, 255, 0.7);
    border-radius: 0 0 0.7em 0.7em;
    color: black;
    font-size: 0.7em;
    margin: 0 0.7em;
    text-align: right;
  }
</style>
<figure>
  <img />
  <figcaption id="caption"></figcaption>
  <figcaption id="credit">
    <a id="author"></a>&nbsp; <a id="licence"></a>&nbsp;
    <a id="image-description">via Wikimedia Commons</a>
  </figcaption>
</figure>
`;var l=class extends HTMLElement{constructor(){super();this.attachShadow({mode:"open"}),this.caption=""}static get observedAttributes(){return["title","caption"]}attributeChangedCallback(e,n,t){n!==t&&(this[e]=t)}get title(){return this.getAttribute("title")}set title(e){this.setAttribute("title",e)}get caption(){return this.getAttribute("caption")}set caption(e){this.setAttribute("caption",e)}async fetchData(e){let n=`Image:${e}`,t="https://commons.wikimedia.org/w/api.php",a={action:"query",format:"json",prop:"imageinfo",iiprop:"extmetadata",titles:n};t=t+"?origin=*",Object.keys(a).forEach(function(i){t+="&"+i+"="+a[i]});let o=await fetch(t).then(function(i){return i.json()}).then(function(i){let c=i.query.pages,u=Object.keys(c)[0];return c[u].imageinfo[0].extmetadata}).catch(function(i){console.error(i)}),r=await fetch(`https://api.wikimedia.org/core/v1/commons/file/${n}`).then(function(i){return i.json()}).then(function(i){return i}).catch(function(i){console.error(i)});return Object.assign(o,r)}async connectedCallback(){this.style.display="block";let e=await this.fetchData(this.title),n=s.content,t=n.querySelector("img");e.original.url.endsWith(".svg")?t.src=e.original.url:t.src=e.preferred.url,this.caption?t.alt=this.caption:e.ImageDescription?t.alt=e.ImageDescription.value:e.ObjectName?t.alt=e.ObjectName.value:t.alt="Image libre issue de Wikimedia commons",n.querySelector("#image-description").href=e.file_description_url;let a=n.querySelector("#licence");e.LicenseUrl&&e.LicenseUrl.value?a.href=e.LicenseUrl.value:e.License&&e.License.value==="pd"&&(a.href="https://fr.wikipedia.org/wiki/Domaine_public_(propri%C3%A9t%C3%A9_intellectuelle)"),a.innerHTML=e.LicenseShortName.value;let o=n.querySelector("#author");e.AttributionRequired.value==="true"?(o.innerHTML="&copy;&nbsp;",e.Attribution&&e.Attribution.value&&(o.innerHTML+=e.Attribution.value)):o.innerHTML="";let r=n.querySelector("#caption");this.caption?r.innerHTML=this.caption:r.innerHTML="",this.shadowRoot.appendChild(n.cloneNode(!0))}};customElements.define("wc-wikimage",l);export{l as WCwikimage};
