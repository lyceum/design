# A web component for images from wikimedia commons

## Installation

_Installation_

```sh
npm i wc-wikimage
```

_Import from NPM_

```html
<script type="module" src="node_modules/wc-wikimage/index.js"></script>
```

_Import from CDN_ **TODO**

```html
<script
  type="module"
  src="https://cdn.jsdelivr.net/???gh/????/wc-wikimage/index.js"
></script>
```

## Usage

Two attributes are handled:

- `title`: mandatory allow to fetch data from wikimedia apis.
- `caption`: will add a caption to the figure.

```html
<wc-wikimage title="Arts_et_Metiers_Pascaline_dsc03869.jpg"></wc-wikimage> 
<wc-wikimage title="Bistability_graph.svg"></wc-wikimage>
<wc-wikimage title="WheelGraph.png" caption="wheel graphs" ></wc-wikimage>
```
## Special thanks

- Web component architecture: https://github.com/vanillawc/
