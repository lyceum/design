# A web component for quiz using markdown gfm markdown

## Installation

_Installation_

```sh
npm i wc-quiz
```

_Import from NPM_

```html
<script type="module" src="node_modules/wc-quiz/index.js"></script>
```

_Import from CDN_ **TODO**

```html
<script
  type="module"
  src="https://cdn.jsdelivr.net/???gh/????/wc-quiz/index.js"
></script>
```

## Usage

<!--
Attributes

- title - the title that displays on the card
- type - the content type ()
-->

<!--
Properties

- title - the title that displays on the card
- type - the content type ()
-->

### Basic Usage

<!-- description -->

```html
<!-- code sample -->
```

## Special thanks

- Web component architecture: https://github.com/vanillawc/wc-markdown/

## micromark-plugins

This web componenent uses `micromark` to render markdown to html in the browser _lightly_, it uses:

- `micromark-extension-math` associated with kayex to render tex math to mathhml.
- micromark-extension-frontmatter: TODO Utile ou non?

## References

- https://github.com/syntax-tree/mdast
- https://github.com/syntax-tree/hast
- https://webcomponents.dev/blog/all-the-ways-to-make-a-web-component/
- https://www.sarasoueidan.com/blog/inclusively-hiding-and-styling-checkboxes-and-radio-buttons/

Progressbar: https://getbootstrap.com/docs/5.0/components/progress/#keyframes
