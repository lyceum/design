# Design

Design, logos web-components pour le site <https://www.lyceum.fr>.

## web-components

- [`wc-quiz`](./components/wc-quiz/README.md): un composant web pour créer des
  quiz de type QCU.
- [`wc-wikimage`](./components/wc-wikimage/README.md): un composant web pour
  importer des images de wikimedia libres de droi.

Pour les publier:

- lancer le script `bash prepare-to-publish`
- puis `npm publish`

## Favicon

Utilise la police EBGaramond.

Pour avoir toutes les versions disponibles:

    fc-cat|grep 'EBGaramond'

Généré en grace à luatex en utilisant la police EB-Garamond Initials, puis convertie en png. Selon les détails de [developers google](https://developers.google.com/web/fundamentals/design-and-ux/browser-customization/)

Les fichiers images et la template jinja d'import sont ensuite intégrés au theme pelican grâce au makefile du dossier `favicon`.

## Couleurs

Couleur primaire à partir de laquelle est créée le logo: #003cba

Puis palette grâce à adobe color...
