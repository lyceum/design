# logos lyceum.fr

Création du svg avec code et/ou inkscape. La version finale est la version avec `-code` nettoyée.

## Export des textes en chemin grâce à inkscape.


```
for f in in *-code.svg; do
    inkscape $f --export-text-to-path --export-plain-svg=${f%-code.svg}.svg
done
```

Conversion de l'icone en png pour le webmanifest.

    convert icon.svg -resize 2048x2048 icon.png

Copie vers gatsby:

```
cp *.svg ~/Documents/git/0lyceum/gatsby-starter-default/src/assets
rm ~/Documents/git/0lyceum/gatsby-starter-default/src/assets/*-code.svg
rm ~/Documents/git/0lyceum/gatsby-starter-default/src/assets/*-inkscape.svg
```
