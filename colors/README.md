# palette de couleurs

à, partir des images de base j'ai crée une tile avec pdfjam puis réduit le nb de couleurs avec imagemagick:

    convert tile.png +dither -colors 32 tile-32.jpg

Récupérer les couleurs
    convert tile-32.jpg  -format %c -depth 8  histogram:info:histogram_image.txt
    sort -n histogram_image.txt | tail -32 > palette-32.txt

## Customisation de bootstrap

Il me faut 10 couleurs:

```sh
ncolors=10
convert tile.png +dither -colors $ncolors tile-$ncolors.jpg
convert tile-$ncolors.jpg  -format %c -depth 8  histogram:info:histogram_image.txt
sort -n histogram_image.txt | tail -$ncolors > palette-$ncolors.txt
```

On recrée une palette scss avec ^\\s+([0-9]+)._(#[0-9a-f]{6})._$

**Voir le fichier bootstrap colors de www.**
